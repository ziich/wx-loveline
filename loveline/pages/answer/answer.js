// pages/answer/answer.js
const app = getApp()
const myRequest = require('../../lib/api/request')
Page({

  /**
   * 页面的初始数据
   */
  onPullDownRefresh: function () {

    wx.stopPullDownRefresh()
  },
  data: {
    submitButtonDisable: false
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("in answer.js")
    let that = this
    myRequest.get({
      path: "prompts/" + app.globalData.promptId,
      success: function (res) {
        // console.log(132323, res)
        that.setData({
          content: res.data.content
        })
      }
    })

  },

  formSubmit: function (e) {
    this.setData({
      submitButtonDisable: true
    })
    let page = this
    if (e.detail.value.prompt_answer === "") {
      wx.showModal({
        title: 'Please enter your answer',
        content: '',
      });
      page.setData({
        submitButtonDisable: false
      })
    } else {
      wx.showToast({ title: 'Sending...', icon: 'loading', duration: 5000 })

      myRequest.post({
        path: "prompt_lovelines",
        data: {
          loveline_id: app.globalData.lovelineId,
          prompt_id: app.globalData.promptId
        },
        success: function (res) {
          myRequest.post({
            path: `prompt_lovelines/${res.data.id}/answers`,
            data: {
              content: e.detail.value.prompt_answer,
              user_id: app.globalData.userId,
            },
            success(res) {
              wx.reLaunch({
                url: '/pages/loveline/loveline'
              })
            }
          })
        }
      })
    }
    
    
  }
})  
