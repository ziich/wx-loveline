const myRequest = require('../../lib/api/request')
const app = getApp()
const passphrase = 'smartgee'
Page({
  onPullDownRefresh: function () {

    wx.stopPullDownRefresh()
  },

  onLoad: function (option) {
    app.globalData.sender_id = option.sender_id
    app.globalData.loveline_id = option.loveline_id

    const host = 'https://loveline.wogengapp.cn/'
    console.log('processing to login')
    const that = this

    wx.login({
      success: res => {
        wx.request({
          url: host + 'login',
          method: 'post',
          data: {
            code: res.code,
            passphrase
          },
          success: res => {
            getApp().globalData.full_user_id = res.data.lovelineId
            getApp().globalData.userId = res.data.userId
            that.setData({
              buttonClickable: true
            })
          }
        })

      }
    })
  },
  getUserInfo: function (e) {
    const that = this
    // console.log(222222, e)
    app.globalData.userInfo = e.detail.userInfo

    this.setData({
      userInfo: e.detail.userInfo
    })
    myRequest.put({
      path: `users/${app.globalData.userId}`,
      data: {
        nickname: e.detail.userInfo.nickName,
        gender: e.detail.userInfo.gender,
        avatar_url: e.detail.userInfo.avatarUrl
      },
      success: function () {
        wx.reLaunch({
          url: '/pages/loveline/loveline'
        })
      }
    })
  },
  /**
   * 页面的初始数据
   */
  data: {

  },
  jumpToLoveline: function () {
    wx.navigateTo({
      url: '/pages/loveline/loveline',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})