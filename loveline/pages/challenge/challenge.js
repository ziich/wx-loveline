// pages/challenge/challenge.js
const app = getApp()
const myRequest = require('../../lib/api/request')

Page({


  onPullDownRefresh: function () {
    let that = this
    this.setData({ showHeart: true })
    // console.log("Yeahhhhh pull down")
    this.audioCtx = wx.createAudioContext('myAudio')
    this.audioCtx.setSrc('../../lib/assets/heartbeat.mp3')
    this.audioCtx.play()
    let audio = this.audioCtx
    that.onLoad()
    setTimeout(function () {
      audio.pause()
      wx.stopPullDownRefresh()
      that.setData({ showHeart: false })
    }, 2000)
  },


  data: {
    items: [],
    showHeart: false
  },



  onLoad: function (options) {
    console.log("in challenge")
    let that = this
    myRequest.get({
      path: 'lovelines/' + app.globalData.lovelineId,
      success: function (res) {
        console.log(1313133113, res)
        
        app.globalData.lovelineScore = res.data.loveline_score.score
        app.globalData.lovelinePrompts = res.data.prompts
        that.setData({ 
          lovelinePrompts: app.globalData.lovelinePrompts.reverse(),
          lovelineCounterPrompts: app.globalData.lovelinePrompts.length,
          lovelineScore: app.globalData.lovelineScore
          
          })
        console.log(1313133113, app.globalData.lovelinePrompts)



          

        let completeAnswers = true
        let lovelinePrompts = app.globalData.lovelinePrompts
        console.log(12344444, lovelinePrompts)
        let lovelinePromptsCompleted = 0
        lovelinePrompts.forEach((lovelinePrompt) => {
          if (lovelinePrompt.answers.length === 2) {
            lovelinePromptsCompleted += 1;
          }else{
            completeAnswers=false;
          }
          console.log(3232323, lovelinePromptsCompleted)
        })
        app.globalData.lovelinePromptsCompleted = lovelinePromptsCompleted
        that.setData({ lovelinePromptsCompleted })
        app.globalData.completeAnswers = completeAnswers
        that.setData({ completeAnswers })
        console.log(121212112, app.globalData.completeAnswers)
      }
    })
  },

  showchallenge: function (e) {
    console.log(123123123, e)
    app.globalData.current_prompt_id = e.currentTarget.id
    wx.navigateTo({
      url: '/pages/showchallenge/showchallenge',
    })
  },

})


