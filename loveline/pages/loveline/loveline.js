// pages/loveline/loveline.js
const app = getApp()
const myRequest = require('../../lib/api/request')
Page({
  
  breakup: function(){
    wx.navigateTo({
      url: '/pages/breakup/breakup?i=' + Math.floor((Math.random() * 4))
    })
  },

  onPullDownRefresh: function () {
    let that = this
    this.setData({showHeart: true})
    // console.log("Yeahhhhh pull down")
    this.audioCtx = wx.createAudioContext('myAudio')
    this.audioCtx.setSrc('../../lib/assets/heartbeat.mp3')
    this.audioCtx.play()
    let audio = this.audioCtx
    that.onLoad()
    setTimeout(function() {
      audio.pause()
      wx.stopPullDownRefresh()
      that.setData({ showHeart: false })
    }, 2000)
  },

  onShareAppMessage: function(res) {
    let sender_id = app.globalData.userId
    wx.showShareMenu({
      withShareTicket: true
    })
    return {
      title: "Ready to love?",
      path: 'pages/login/login?sender_id=' + sender_id + '&&loveline_id=' + app.globalData.lovelineId,
      imageUrl: '../../lib/assets/share2.png'
    }
  },
  

  data: {
    items: [],
    showHeart: false
  },

  onLoad: function (option) {
    
    // this.setData({ completeAnswers: app.globalData.completeAnswers})
    console.log(1212121, app.globalData)

    // console.log(9999,completeAnswers)
    let name = app.globalData.userInfo.nickName
    // console.log(name)
    let avatar = app.globalData.userInfo.avatarUrl
    this.setData({
      name,
      avatar
    })
    let aha = this
    myRequest.get({
      path: 'users/' + getApp().globalData.userId,
      success: function(res){
        console.log("here is the first request")
        console.log(88,res)
        if (res.statusCode != 200){
          wx.navigateTo({
            url: '/pages/login/login',
          })
          wx.showModal({
            title: 'loveline doesn\'t exist!',
          content: 'Sorry, your partner has ended the loveline',
          })
          
        }else {
          
        
      

    // this.setData({ answernum: app.globalData.promptAccepted })


    // console.log(10101011,app.globalData)

    if (typeof(app.globalData.loveline_id) === "undefined") {
      console.log("normal enter")
      let page = aha
      // console.log(111, app)
      // console.log(222, this.data.items)
      // this.setData({ pictures: options.leanCloudImage })

      myRequest.get({
        path: `users/${app.globalData.userId}`,
        success: function (res) {
          page.setData({ loveline_id: res.data.user_loveline.id })
          app.globalData.lovelineId = page.data.loveline_id
          // console.log(12345678, app.globalData)
          // console.log(8989, res)


          myRequest.get({
            path: "lovelines/" + page.data.loveline_id,
            success: function (res) {
              console.log(444, res)
              app.globalData.current_score = res.data.loveline_score.score
              page.setData({
                loveline_date: res.data.date,
                loveline_score: res.data.loveline_score.score
              })
              app.globalData.lovelineDate = res.data.date

              page.setData({ loveline_prompts: res.data.prompts })
              app.globalData.lovelinePrompts = res.data.prompts


              let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
              // console.log(565656, oneDay)
              let firstDate = new Date(app.globalData.lovelineDate);
              console.log(545454, firstDate)


              let secondDate = new Date();
              console.log(343434344, secondDate)



              let diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));



              page.setData({
                firstDate,
                diffDays
              })



              let partner_id
              page.setData({
                single_rider: res.data.lovelines_users.length === 1,
                total_posts: res.data.lovelines_posts.length,
                answer_given: res.data.prompts.length

              })
              // console.log(page.data.single_rider)
              // console.log(res.data.lovelines_users.length)
              page.setData({ posts: res.data.lovelines_posts })
              console.log(1010101010, res.data.lovelines_users[0].id)
              if (page.data.single_rider) {

              } else {
                if (app.globalData.userId === res.data.lovelines_users[0].id) {
                  partner_id = res.data.lovelines_users[1].id
                } else {
                  partner_id = res.data.lovelines_users[0].id
                }
                // console.log(partner_id)
                myRequest.get({
                  path: "users/" + partner_id,
                  success: function (res) {
                    // console.log(res)
                    app.globalData.partner_gender = res.data.gender
                    page.setData({
                      partner_nickname: res.data.nickname,
                      partner_avatar_url: res.data.avatar_url

                    })
                  }
                })
              }
              page.setData({ comments: res.data.lovelines_posts })
              app.globalData.comments = page.data.comments
              // console.log(1994, app.globalData.comments)

              // check for incomplete answer
              let answersAllDone = true
              let lovelinePrompts = app.globalData.lovelinePrompts
              let lovelinePromptsCompleted = 0
              lovelinePrompts.forEach((lovelinePrompt) => {
                if (lovelinePrompt.answers.length === 1) {
                  answersAllDone = false;
                }
              })
              page.setData({ answersAllDone })

              let orderedPosts = {}
              let dates = []
              let posts = page.data.posts
              posts.forEach((post) => {
                if (!dates.includes(post.Date)) {
                  dates.push(post.Date)
                  dates.sort();
                  dates.reverse();
                }
                if (orderedPosts[post.Date] === undefined) {
                  orderedPosts[post.Date] = [post]
                } else {
                  orderedPosts[post.Date].unshift(post)
                }

                // console.log(7777, res.data.lovelines_posts.length)
                page.setData({
                  orderedPosts,
                  dates,
                  diffDays

                })

              })
            }
          })
        }
      })
    } else {
      let haha = this
      console.log("from share join the loveline")
      myRequest.get({
        path: `lovelines/${app.globalData.loveline_id}`,
        success: function (res) {
          let that = haha
          app.globalData.loveline_you_wanto_join_in_length = res.data.lovelines_users.length
          myRequest.get({
            path: 'users/' + getApp().globalData.userId,
            success: function (res) {
              let ha= that
              app.globalData.your_loveline_id = res.data.user_loveline.id

              if (app.globalData.loveline_you_wanto_join_in_length == 2 && app.globalData.your_loveline_id != app.globalData.loveline_id) {
                wx.showModal({
                  title: 'Oh, what the fck!',
                  content: 'Please do not join other people\'s loveline, they are in love',
                })
                console.log("wahahahaa");
                app.globalData.loveline_id = undefined
              } else {
                myRequest.put({
                  path: `users/${app.globalData.userId}`,
                  data: {
                    loveline_id: app.globalData.loveline_id
                  },
                  success: function(){
                    let page =  aha
                    // console.log(111, app)
                    // console.log(222, this.data.items)
                    // this.setData({ pictures: options.leanCloudImage })

                    myRequest.get({
                      path: `users/${app.globalData.userId}`,
                      success: function (res) {
                        page.setData({ loveline_id: res.data.user_loveline.id })
                        app.globalData.lovelineId = page.data.loveline_id
                        // console.log(12345678, app.globalData)
                        // console.log(8989, res)


                        myRequest.get({
                          path: "lovelines/" + page.data.loveline_id,
                          success: function (res) {
                            console.log(444, res)
                            app.globalData.current_score = res.data.loveline_score.score
                            page.setData({
                              loveline_date: res.data.date,
                              loveline_score: res.data.loveline_score.score
                            })
                            app.globalData.lovelineDate = res.data.date

                            page.setData({ loveline_prompts: res.data.prompts })
                            app.globalData.lovelinePrompts = res.data.prompts


                            let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                            // console.log(565656, oneDay)
                            let firstDate = new Date(app.globalData.lovelineDate);
                            console.log(545454, firstDate)


                            let secondDate = new Date();
                            console.log(343434344, secondDate)



                            let diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));



                            page.setData({
                              firstDate,
                              diffDays
                            })



                            let partner_id
                            page.setData({
                              single_rider: res.data.lovelines_users.length === 1,
                              total_posts: res.data.lovelines_posts.length,
                              answer_given: res.data.prompts.length

                            })
                            // console.log(page.data.single_rider)
                            // console.log(res.data.lovelines_users.length)
                            page.setData({ posts: res.data.lovelines_posts })
                            console.log(1010101010, res.data.lovelines_users[0].id)
                            if (page.data.single_rider) {

                            } else {
                              if (app.globalData.userId === res.data.lovelines_users[0].id) {
                                partner_id = res.data.lovelines_users[1].id
                              } else {
                                partner_id = res.data.lovelines_users[0].id
                              }
                              // console.log(partner_id)
                              myRequest.get({
                                path: "users/" + partner_id,
                                success: function (res) {
                                  // console.log(res)
                                  app.globalData.partner_gender = res.data.gender
                                  page.setData({
                                    partner_nickname: res.data.nickname,
                                    partner_avatar_url: res.data.avatar_url

                                  })
                                }
                              })
                            }
                            page.setData({ comments: res.data.lovelines_posts })
                            app.globalData.comments = page.data.comments
                            // console.log(1994, app.globalData.comments)

                            let orderedPosts = {}
                            let dates = []
                            let posts = page.data.posts
                            posts.forEach((post) => {
                              if (!dates.includes(post.Date)) {
                                dates.push(post.Date)
                                dates.sort();
                                dates.reverse();
                              }
                              if (orderedPosts[post.Date] === undefined) {
                                orderedPosts[post.Date] = [post]
                              } else {
                                orderedPosts[post.Date].unshift(post)
                              }

                              // console.log(7777, res.data.lovelines_posts.length)
                              page.setData({
                                orderedPosts,
                                dates,
                                diffDays

                              })

                            })
                          }
                        })
                      }
                    })
                  }


                })
              }
            }
          })
          // get current userid, get his currentusers lovelineId
          // get lovelineid of user he's trying to join
          
        }
      })


      
      
    }


      }








    
    // console.log(12345, app.globalData.userInfo.nickName)
      }
    })
  },
  



























  show: function (e) {
    app.globalData.current_post_id = e.currentTarget.id
    // console.log(99999, app.globalData.current_post_id)
    // console.log("Below is the id of thing we clicked:")
    // console.log(888, e.currentTarget.dataset.orderedPosts)
    // console.log("Below is the global data id being passed to show:")
    // console.log(app.globalData.current_post_id)
    console.log(666, e)
    
    wx.navigateTo({
      url: '/pages/show/show',
    })
  },

  newPost: function (e) {
    wx.navigateTo({
      url: '/pages/post/post',
    })
  },
  prompt: function(e){
    wx.navigateTo({
      url: '/pages/prompt/prompt',
    })
  },
   challengesDone: function (e) {
    wx.navigateTo({
      url: '/pages/challenge/challenge',
    })
  }
})