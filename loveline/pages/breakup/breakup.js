// pages/breakup/breakup.js
const app = getApp()
const myRequest = require('../../lib/api/request')
Page({
  data: {
    sayings: ["Love isn't easy. Don't give up in the face of difficulty. --Gee",
              "The road of love is never smooth. You may never meet someone like this again. --Gee",
              "Love yourself first and you'll never be disappointed. --Chris",
              "Love is me, myself, and I. --Fabio"]
  },
  onLoad: function (option) {
    this.setData({
      i: option.i
    })
  },

  bye: function(){
    wx.showModal({
      title: 'Warning',
      content: 'Leaving this loveline will delete all associated content FOREVER and cannot be undone.',
      success: function (res) {
        if (res.confirm) {
          console.log('confirm breakup')
          myRequest.delete({
            path: `lovelines/${app.globalData.lovelineId}`,
            success: function (res) {
              wx.navigateTo({
                url: '/pages/login/login',
              })
            }

          })
        } else if (res.cancel) {
          console.log('cancel breakup')
        }
      }
    })

    
  }
})
