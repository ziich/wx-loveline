// pages/prompt/prompt.js
const app = getApp()
const myRequest = require('../../lib/api/request')
Page({

  /**
   * 页面的初始数据
   */
  onPullDownRefresh: function () {

    wx.stopPullDownRefresh()
  },
  data: {
    prompts: [],

    promptId: 0
  },

  acceptPrompt: function (e) {
    let page = this
    app.globalData.promptId = this.data.prompts[this.data.i].id
        wx.navigateTo({
          url: '/pages/answer/answer'
        })

    
  },

  declinePrompt: function (){
    let page = this
      
    page.setData({
      i: Math.floor(Math.random() * page.data.prompts.length)
    })
      
    // console.log(999, this.page.data.i)
  },

  // declinePrompt: function (){
  //   if (this.data.i === 9 )
  //   {
  //     this.setData({
  //       i: 0
  //     })
  //   }else {
  //   this.data.i +=1;
  //   let page = this
  //   this.setData({
  //     i: page.data.i
  //   })
  // }
  // },

   /**
   * 生命周期函数--监听页面加载
   */
  
  onLoad: function () {
    console.log("in prompt.js")
    let page = this
    myRequest.get({
      path:  "prompts",
      success: function (res) {
        console.log(132323,res)
        app.globalData.numofPrompts = res.data.prompts.length
        console.log(app.globalData.numofPrompts)
        page.setData({
          prompts: res.data.prompts,
          i: Math.floor(Math.random() * app.globalData.numofPrompts)
        })
        
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})