const app = getApp()
const myRequest = require('../../lib/api/request')
// pages/showchallenge/showchallenge.js
Page({
  
  data: {
    
    
  },
  onPullDownRefresh: function () {

    wx.stopPullDownRefresh()
  },


  onLoad: function (options) {
    console.log("in showchallenge")
    let that = this
    console.log(1212121222, app.globalData)
    myRequest.get({
      path: 'prompt_lovelines/' + app.globalData.current_prompt_id,
      success: function (res) {
        
        console.log(5555551, res.data)
        app.globalData.current_prompt_score = res.data.question.score
        console.log(232323, res.data.answers.length)
        app.globalData.all = res.data
        that.setData({ all: app.globalData.all })
        console.log(121212, app.globalData.all)

        that.setData({ one_answer: res.data.answers.length === 1, })
        // that.setData({ my_answer: res.data.answers.length === 1, })
        app.globalData.all.answers.forEach((answer) => {
          if (answer.answer_user.id === app.globalData.userId) {
            that.setData({ my_answer: false });
          } else {
            that.setData({ my_answer: true });
          }
          
        })
      }
    })
  },








  formSubmit: function (e) {
    this.setData({
      submitButtonDisable: true
    })
    console.log(19191919, e)
    let page = this


    if (e.detail.value.comment === "") {
      wx.showModal({
        title: 'Please enter your answer',
        content: '',
      });
      page.setData({
        submitButtonDisable: false
      })
    } else {
      wx.showToast({ title: 'Sending...', icon: 'loading', duration: 1000 })

      myRequest.post({
        path: 'prompt_lovelines/' + app.globalData.current_prompt_id + '/answers',
        data: {
          content: e.detail.value.comment,
          user_id: app.globalData.userId,
        },
        success(res) {

          console.log(9090, app.globalData.current_score)
          console.log(9090, app.globalData.current_prompt_score)
          myRequest.put({
            path: 'lovelines/' + app.globalData.lovelineId,
            data: {
              score: (app.globalData.current_score + app.globalData.current_prompt_score)
            },
            success: function(){
              wx.reLaunch({
                url: '/pages/loveline/loveline'
              })
            }
          })
        }
      })
    }
  }
})