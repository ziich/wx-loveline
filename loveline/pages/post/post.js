// pages/post / post.js
const app = getApp()
const myRequest = require('../../lib/api/request');
const AV = require('../../utils/av-weapp-min.js');

Page({

  onPullDownRefresh: function () {

    wx.stopPullDownRefresh()
  },
  data: {
    items: [],
    submitButtonDisable : false
  },
  onLoad: function () {
    app.globalData.pictures = ""
  },

  takePhoto: function () {
    let that = this
    wx.chooseImage({
      count: 4,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {

        let tempFilePaths = res.tempFilePaths;
        console.log(55, res.tempFilePaths)

        let tempFilePathsLength = tempFilePaths.length;
        console.log(66, tempFilePathsLength)

        that.setData({
          imagess: res.tempFilePaths
        }) 
        // let tempFilePathsFirst = tempFilePaths[0] 
        // let tempFilePathsSecond = tempFilePaths[1] 
        // let tempFilePathsThird = tempFilePaths[2] 

        // app.globalData.pictures = tempFilePaths
        console.log(3443434, app.globalData.pictures)



        res.tempFilePaths.map(tempFilePath => () => new AV.File('filename', {
          blob: {
            uri: tempFilePath,
          },
        }).save()).reduce(
          (m, p) => m.then(v => AV.Promise.all([...v, p()])),
          AV.Promise.resolve([])
        ).then(function (files) {
          let myImages = []
          files.map(file => {
            // file.url()
            myImages.push(file.url())
          })
          app.globalData.pictures = myImages
          console.log(44444, app.globalData.pictures)
        }).catch(console.error);




        that.uploadPromise(tempFilePaths[0]).then(res => {
          return res
          console.log(88888, res)
        }).then(res => {
          let url = `/pages/index/index?leanCloudImage=${res}`
          // app.globalData.pictures = res
          console.log(777, app.globalData.pictures)
        })
      }
    });
  },
  uploadPromise: function (tempFilePath) {
    return new Promise((resolve, reject) => {
      new AV.File('file-name', {
        blob: {
          uri: tempFilePath,
        },
      }).save()
        .then(file => resolve(file.url()))
        .catch(e => reject(e));
    })
  },
  data: {},


  formSubmit: function (e) {
    this.setData({
      submitButtonDisable: true
    })
    let page = this
    // console.log(9898989, e)
    if (e.detail.value.title === "" || e.detail.value.content === "") {
      wx.showModal({
        title: 'Please enter post title and post description',
        content: '',
      });
      page.setData({
        submitButtonDisable: false
      })
    } else {
      let images = []
      images.push(app.globalData.pictures)
      console.log(999, images)
      wx.showToast({ title: 'Sending...', icon: 'loading', duration: 5000 })
      // Post new  to API

      // console.log(1994, e.detail.value.title)

      myRequest.post({
        path: 'posts',
        data: {
          title: e.detail.value.title,
          content: e.detail.value.content,
          user_id: app.globalData.userId,
          loveline_id: app.globalData.lovelineId,
          image_url: app.globalData.pictures
        },
        success(res) {
          myRequest.put({
            path: 'lovelines/' + app.globalData.lovelineId,
            data: {
              score: (app.globalData.current_score + 10)
            },
            success(res) {
              wx.reLaunch({
                url: '/pages/loveline/loveline'
              })
            }
          })
        }
      })




      
    }
    

    
      

  }
})