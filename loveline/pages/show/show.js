// pages/show/show.js
const app = getApp()
const myRequest = require('../../lib/api/request')
const passphrase = 'smartgee'
Page({


  data:{
    indicatorDots: true,  
  },

  onPullDownRefresh: function () {

    wx.stopPullDownRefresh()
  },
onLoad:function (options) {
  console.log("in show.js")
  let that = this
  myRequest.get({
   path:'posts/' + app.globalData.current_post_id,
    success: function (res) {
      console.log(res.data)
      app.globalData.images = res.data.image_url
      that.setData({ myImages: app.globalData.images})
      that.setData({ post: res.data })
      that.setData({ comments: res.data.comments.reverse()})
      that.setData({imaglist: res.data.image_url})
      passphrase
      

    }
  })

  console.log(9090,app.globalData.images)
  // let comments = app.globalData.comments[0].comments[0].content
  // let comments = app.globalData.comments
  // console.log(5454,comments)


//   this.setData({
//     comments,
//   })

},

  previewImage: function (e) {
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, 
      urls: this.data.post.image_url
    })

  },  


deletePost: function (e) {
  wx.showModal({
    title: 'Delete Post?',
    content: 'Are you sure you want to delete this post?',
    success: function (res) {
      if (res.confirm) {
        let page = this
        myRequest.delete({
          path: `posts/${e.currentTarget.id}`,

          success: function (res) {
            console.log('deleted')
          },
          fail: function (res) {
            console.log('Error')
          },
        })
        wx.reLaunch({
          url: '/pages/loveline/loveline',
        })
      }
    }
  })
},

  formSubmit: function(e) {
    console.log(19191919,e)
    let page = this
    wx.showToast({ title: 'Sending...', icon: 'loading', duration: 1000 })
    
    myRequest.post({
      path: 'posts/' + app.globalData.current_post_id + '/comments',
      data: {
        content: e.detail.value.comment,
        user_id: app.globalData.userId,
        post_id: app.globalData.lovelineId.id,
         
      },
      success(res) {
        console.log(res)
        page.onLoad()
      }
    })
    setTimeout(function () {

    }, 1000)
  },

  back: function (e) {
    wx.navigateTo({
      url: '/pages/loveline/loveline',
    })
  },

  


})